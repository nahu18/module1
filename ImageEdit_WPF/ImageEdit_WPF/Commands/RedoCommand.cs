﻿
using System.Windows.Input;

namespace ImageEdit_WPF.Commands {
    public class RedoCommand {
        private static readonly RoutedUICommand m_redo;

        public static RoutedUICommand Redo {
            get { return m_redo; }
        }

        static RedoCommand() {
            InputGestureCollection gestures = new InputGestureCollection();
            gestures.Add(new KeyGesture(Key.Y, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+Y"));
            m_redo = new RoutedUICommand("Redo", "Redo", typeof (RedoCommand), gestures);
        }
    }
}
