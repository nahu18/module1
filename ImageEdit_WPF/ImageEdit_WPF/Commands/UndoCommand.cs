﻿
using System.Windows.Input;

namespace ImageEdit_WPF.Commands {
    public class UndoCommand {
        private static readonly RoutedUICommand m_undo;

        public static RoutedUICommand Undo {
            get { return m_undo; }
        }

        static UndoCommand() {
            InputGestureCollection gestures = new InputGestureCollection();
            gestures.Add(new KeyGesture(Key.Z, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+Z"));
            m_undo = new RoutedUICommand("Undo", "Undo", typeof (HelpCommand), gestures);
        }
    }
}
