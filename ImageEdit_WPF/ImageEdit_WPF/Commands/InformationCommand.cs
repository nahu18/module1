﻿using System.Windows.Input;

namespace ImageEdit_WPF.Commands {
    public class InformationCommand {
        private static readonly RoutedUICommand m_information;

        public static RoutedUICommand Information {
            get { return m_information; }
        }

        static InformationCommand() {
            InputGestureCollection gestures = new InputGestureCollection();
            gestures.Add(new KeyGesture(Key.I, ModifierKeys.Control, "Ctrl+I"));
            m_information = new RoutedUICommand("Information", "Information", typeof (InformationCommand), gestures);
        }
    }
}
